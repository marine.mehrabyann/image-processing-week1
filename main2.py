# Problem 2.
# Count the vowels in a string.

def vowel_count(str):

    count = 0
    vowel = set("aeiouAEIOU") # {'a','e','i','o','u','A','E','I','O','U'}

    for i in str:
        if i in vowel:
            count = count + 1

    return "The number of vowels is  :", count

str = input("Please enter a line : \n")
print(vowel_count(str))
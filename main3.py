#Problem 3.
#Find in file the given string with and substitute with the other one that is  provided from command line.

print ("Text to search for:")
textToSearch = input( "> " )
print ("Text to replace it with:")
textToReplace = input( "> " )

new_file=open('string.txt', 'w')
new_file.write('''
 Python is a high-level, general-purpose programming language.
 Its design philosophy emphasizes code readability with the use of significant indentation. 
 Its language constructs and object-oriented approach aim to help programmers write clear,
 logical code for small- and large-scale projects.

 Python is dynamically-typed and garbage-collected. 
 It supports multiple programming paradigms, including structured (particularly procedural),
 object-oriented and functional programming. It is often described as a "batteries included"
 language due to its comprehensive standard library
'''
)
new_file.close()

print(open('string.txt').read())

with open('string.txt', 'r') as file :
    filedata = file.read()

filedata = filedata.replace(textToSearch, textToReplace)

with open('string.txt', 'w') as file:
    file.write(filedata)

print(open('string.txt').read())



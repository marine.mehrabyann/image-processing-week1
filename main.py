# Problem 1.
# Read from a csv file car parking database, add support to filter by car model,  issue-date.
import csv
import pandas as pd
header = ['Car mode', 'Date', 'Price','Color']
data = [
    ['Kia','3/9/2011' , 13500, 'Black' ],
    ['Lexus', '3/2/2012',16700,'Green'],
    ['Nissan','3/8/2011', 12400,'Brown' ],
    ['opel', '3/12/2012',10000,'Blue'],
    ['Hyundai','3/5/2011', 14500,'Red'],
    ['Ford', '15/7/2010', 11000, 'Silver'],
    ['BMW','3/02/2018', 22000, 'Purple'],
    ['Ferrari', '3/08/2020', 23500,'White' ],
    ['Jeep', '18/8/2010', 15200, 'Black']
]
with open('car.csv', 'w', newline='') as f:
    writer = csv.writer(f)
    writer.writerow(header)
    writer.writerows(data)


with  open('car.csv', 'r') as file:
    reader=csv.reader(file)
    for row in reader:
        print(row)

filtered_df = pd.read_csv("car.csv", usecols=["Car mode", 'Date'])

print(filtered_df)












#Problem 4.
#From standard input read integer numbers until -1 is met. After that remove
#the most common elements and display on standard output the result and the common  value occurence.


def most_frequent(List):
    counter = 0
    num = List[0]

    for i in List:
        curr_frequency = List.count(i)
        if(curr_frequency > counter):
            counter = curr_frequency
            num = i
    return num


thelist = []
t=True
while t:
    ele = int(input())
    if ele==-1:
        t=False
    else:
        thelist.append(ele)


print(thelist)
a= most_frequent(thelist)

print(a)
print([s for s in thelist if s != a])




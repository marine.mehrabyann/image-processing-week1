# Problem 7.
#  Define a phonebook name, phone number,
#  read from command line a name  and if the name is found print out the phone number.


phonebook = {
    "John" : 938477566,
    "Jack" : 938377264,
    "Ann" : 757662781,
    'Alice':98107593,
    'Max':92104584,
    'Sophia': 88939390
}
name=input("name -> ")
if name in phonebook:
    print(phonebook[name])
else:
    print('Not Found')
